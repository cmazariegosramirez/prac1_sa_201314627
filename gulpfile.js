const gulp = require('gulp');

const tar = require('gulp-tar');
const gzip = require('gulp-gzip');

let fs = require('fs');

let app = require('./package.json');

//estandar semserver para el versionamiento del proyecto
let v1 = parseInt(app.version.split('.')[0]);
let v2 = parseInt(app.version.split('.')[1]);
let v3 = parseInt(app.version.split('.')[2]);

app.version = `${v1}.${v2}.${v3}`;
fs.writeFileSync(__dirname + '/package.json', JSON.stringify(app, null, 4));

//construccion del artefacto definiendo el estandar del semserver en el dist generado
gulp.task('runUploader', async function () {
    gulp.src('./src/**')
        .pipe(tar(`v${app.version}`))
        .pipe(gzip())
        .pipe(gulp.dest('./dist')) 
});